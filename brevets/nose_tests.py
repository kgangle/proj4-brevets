'''
Author Ken Gangle
CiS 322

Nose tests for acp_times.py

'''

import acp_times
import nose
import arrow

def test_short():
    #test for a distance less than 200
    #if distance is 100, the open time should be
    # time plus 2 hours and 56 minutes and 28 seconds
    #and the close time should be
    #time plus 6 hours and 40 minutes

    date = arrow.get('2018-11-11T12:00:00')
    control = 100
    brevet = 200

    assert acp_times.close_time(control, brevet, arrow.get(date)) == date.replace(hours=+6, minutes =+ 40)
    assert acp_times.open_time(control, brevet, arrow.get(date)) == date.replace(hours=+2, minutes =+ 56, seconds =+28)


def test_less_than_400():
    #test for a distance less than 400
    #but more than 200
    #if distance is 300, the open time should be
    # time plus 9 hours and 22 minutes and 30 seconds
    #and the close time should be
    #time plus 20 hours

    date = arrow.get('2018-11-11T12:00:00')
    control = 300
    brevet = 400

    assert acp_times.close_time(control, brevet, arrow.get(date)) == date.replace(hours=+20)
    assert acp_times.open_time(control, brevet, arrow.get(date)) == date.replace(hours=+ 9, minutes =+ 22, seconds =+30)

def test_less_than_600():
    #test for a distance less than 600
    #but more than 400
    #if distance is 450, the open time should be
    # time plus 15 hours
    #and the close time should be
    #time plus 20 hours

    date = arrow.get('2018-11-11T12:00:00')
    control = 450
    brevet = 600

    assert acp_times.close_time(control, brevet, arrow.get(date)) == date.replace(hours=+20)
    assert acp_times.open_time(control, brevet, arrow.get(date)) == date.replace(hours=+ 15)

def test_less_than_1000():
    #test for a distance less than 1000
    #but more than 600
    #if distance is 700, the open time should be
    # time plus 25 hours
    #and the close time should be
    #time plus 61 hours and 15 minutes and 11 seconds

    date = arrow.get('2018-11-11T12:00:00')
    control = 700
    brevet = 1000

    assert acp_times.close_time(control, brevet, arrow.get(date)) == date.replace(hours=+61, minutes =+ 15, seconds =+ 11)
    assert acp_times.open_time(control, brevet, arrow.get(date)) == date.replace(hours=+ 25)

def test_boundary():
    #test for a distance exactly 600
    #if distance is 600, the open time should be
    # time plus 20 hours
    #and the close time should be
    #time plus 40 hours

    date = arrow.get('2018-11-11T12:00:00')
    control = 600
    brevet = 600

    assert acp_times.close_time(control, brevet, arrow.get(date)) == date.replace(hours=+40)
    assert acp_times.open_time(control, brevet, arrow.get(date)) == date.replace(hours=+ 20)




