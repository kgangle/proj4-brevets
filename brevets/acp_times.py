"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions

#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signaztures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    
    """
    MaxSpeeds = {1300: 26, 1000: 28, 600: 30, 400: 32, 200: 34, 0: 15}
    
    if control_dist_km > brevet_dist_km: #if distance is larger than brevet, use brevet distance
        dist = brevet_dist_km
    else:
        dist = control_dist_km

    speed = MaxSpeeds.get(brevet_dist_km)
    time = dist / speed 
    hours = int(time)
    minutes = round( 60*(time-hours))
    seconds = round(((60*(time-hours))-minutes)*60)

    return_time = brevet_start_time.replace(hours =+ hours, minutes =+ minutes, seconds =+ seconds)

    return arrow.get(return_time).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
    speed = MaxSpeeds.get(brevet_dist_km)
   control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    MinSpeeds = {0: 15, 200: 15, 400: 15, 600: 11.428, 1000: 13.333}
    if control_dist_km > brevet_dist_km:
        dist = brevet_dist_km
    else:
        dist = control_dist_km
    
    speed = MinSpeeds.get(brevet_dist_km)
    time = dist / speed
    hours = int(time)
    minutes = round( 60*(time-hours))
    seconds = round( ((60*(time-hours))-minutes)*60)
    return_time = brevet_start_time.replace(hours =+ hours, minutes =+ minutes,seconds =+ seconds)

    
    return arrow.get(return_time).isoformat()
